const express = require('express');
// Require Africa's Talking SDK here ...
const AfricasTalking = require('./africastalking');

const router = express.Router();

// Initialize Africa's Talking SDK here ...
const africasTalking = new AfricasTalking({
  username: 'sandbox',
  apiKey: 'dd60e9c15797d465054d79b5345cb19e1e4778177ec7509800dfe7297d88a78b'
});

const payments = africasTalking.PAYMENTS;

router.get('/', (req, res) => {
  res.render('index');
});

// Payment processing code here ...
router.post('/pay', (req, res) => {
  const phoneNumber = req.body.phoneNumber;
  const productName = 'Web Development Ebook';

  const paymentOptions = {
    productName: productName,
    phoneNumber: phoneNumber,
    currencyCode: 'UGX',
    amount: 50000,
    narration: 'Online store payment',
    metadata: {
      customerEmail: req.body.email
    }
  };
  payments
    .mobileCheckout(paymentOptions)
    .then(response => {
      console.log(JSON.stringify(response, 0, 4));
    })
    .catch(error => {
      console.log(error);
    });
  res.redirect('/processing');
});

// Receive payment notification here ...
router.post('/paymentNotification', (req, res) => {
  console.log(JSON.stringify(req.body, 0, 4));

  // SMS confirmation code here ...
  const sms = africasTalking.SMS;

  res.sendStatus(200);
});

router.get('/processing', (req, res) => {
  res.render('processing');
});

module.exports = router;
