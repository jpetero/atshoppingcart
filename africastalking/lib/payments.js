'use strict';

const unirest = require('unirest');
const validate = require('validate.js');
const _ = require('lodash');

const Common = require('./common');

function Payments(options) {
  this.options = options;
}

Payments.prototype.mobileCheckout = function(params) {
  let options = _.cloneDeep(params);
  let _self = this;
  let validationError;
  

  // Validate params
  let _validateParams = function() {
    const constraints = {
      phoneNumber: function(value) {
        if (validate.isEmpty(value)) {
          return {
            presence: {
              message: 'is required'
            }
          };
        }

        if (!/^\+\d{1,3}\d{3,}$/.test(value)) {
          return {
            format: 'must not contain invalid phone number'
          };
        }

        return null;
      },

      productName: function(value) {
        if (validate.isEmpty(value)) {
          return {
            presence: {
              message: 'is required'
            }
          };
        }
        if (!/\S/.test(value)) {
          return {
            format: 'must not contain invalid productName - eg. Space'
          };
        }

        return null;
      },

      currencyCode: function(value) {
        if (validate.isEmpty(value)) {
          return {
            presence: {
              message: 'is required'
            }
          };
        }

        return null;
      },

      metadata: function(value) {
        if (value && !validate.isObject(value)) {
          return {
            format:
              'metadata need to be a string map; i.e. {"key":"value", "otherKey":"otherValue"}'
          };
        }
        return null;
      },

      amount: function(value) {
        if (validate.isEmpty(value)) {
          return {
            presence: {
              message: 'is required'
            }
          };
        }
        if (!validate.isNumber(value)) {
          return {
            format: 'must not contain invalid amount. Must be a number.'
          };
        }

        return null;
      }
    };

    let error = validate(options, constraints);
    if (error) {
      let msg = '';
      for (let k in error) {
        msg += error[k] + '; ';
      }

      validationError = new Error(msg);
    }
  };

  _validateParams();

  return new Promise(function(resolve, reject) {
    if (validationError) {
      return reject(validationError);
    }

    let body = {
      username: _self.options.username,
      productName: options.productName,
      phoneNumber: options.phoneNumber,
      currencyCode: options.currencyCode,
      providerChannel: options.providerChannel,
      amount: options.amount,
      metadata: options.metadata
    };

    let rq = unirest.post(Common.PAYMENT_URL + '/mobile/checkout/request');
    rq.headers({
      apikey: _self.options.apiKey,
      Accept: _self.options.format,
      'Content-Type': 'application/json'
    });

    rq.send(body);

    rq.end(function(resp) {
      if (resp.status === 201) {
        // API returns CREATED on success
        resolve(resp.body);
      } else {
        reject(resp.body || resp.error);
      }
    });
  });
};

Payments.prototype.checkOut = Payments.prototype.mobileCheckout;

module.exports = Payments;
