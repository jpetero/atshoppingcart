'use strict';

const BASE_DOMAIN = 'africastalking.com';
const BASE_SANDBOX_DOMAIN = 'sandbox.' + BASE_DOMAIN;

const initUrls = function(sandbox) {
  const baseDomain = sandbox ? BASE_SANDBOX_DOMAIN : BASE_DOMAIN;
  const baseUrl = 'https://api.' + baseDomain + '/version1';

  exports.BASE_URL = baseUrl;
  exports.PAYMENT_URL = 'https://payments.' + baseDomain;
};

// no sandbox by default
initUrls(false);

exports.enableSandbox = function() {
  initUrls(true);
};
