'use strict';
const unirest = require('unirest');
const validate = require('validate.js');
const _ = require('lodash');

const Common = require('./common');

const Payment = require('./payments');

function AfricasTalking(options) {
  this.options = _.cloneDeep(options);

  validate.validators.isString = function(value, options, key, attributes) {
    if (validate.isEmpty(value) || validate.isString(value)) {
      // String or null & undefined
      return null;
    } else {
      return 'must be a string';
    }
  };

  const constraints = {
    format: {
      inclusion: ['json', 'xml']
    },
    username: {
      presence: true,
      isString: true
    },
    apiKey: {
      presence: true,
      isString: true
    }
  };

  const error = validate(this.options, constraints);
  if (error) {
    throw error;
  }

  switch (this.options.format) {
    case 'xml':
      this.options.format = 'application/xml';
      break;
    case 'json':
    default:
      this.options.format = 'application/json';
  }

  const isSandbox = this.options.username.toLowerCase() === 'sandbox';
  if (isSandbox) {
    Common.enableSandbox();
  }
  this.PAYMENTS = new Payment(this.options);
}

module.exports = function(options) {
  return new AfricasTalking(options);
};
