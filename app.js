const express = require('express');

const logger = require('morgan');

const bodyParser = require('body-parser');

const handlebars = require('express-handlebars');

const router = require('./router');

const app = express();

// Middlewares
app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.engine('hbs', handlebars({ defaultLayout: 'main', extname: '.hbs' }));

app.set('view engine', 'hbs');

// Set the static folder
app.set('views', `${__dirname}/views`);

app.use(logger('dev'));
app.use(router);

const port = process.env.PORT || 5000;

app.listen(port, () => {
  console.log(`Server running @ http://localhost:${port}`);
});
